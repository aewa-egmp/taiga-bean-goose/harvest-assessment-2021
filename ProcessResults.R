rm(list=ls())
options(max.print = 50000)
library(jagsUI)
# library(coda)
# library(rjags)
# library(runjags)
# load.module("mix")
# load.module("dic")
# list.modules()

# June 2020 population estimates
setwd('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\European Geese\\B Taiga Bean Geese\\2020 Assessment\\Analysis')
load('TBG_IPM_deter-proj_13May2020(c).RData')
old.Nm.est=Nm.est[1:25,3:6]
rm(Nm.est)

# May 2021 estimates
setwd('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\European Geese\\B Taiga Bean Geese\\2021 Assessment')
source('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\MyFunctions.R')
load('SubspeciesIPM_jagsUI_4May2021.RData')

# Compare old (with tundra) & new pop (without) size in March
par(mfrow=c(1,1),mar=c(4,5,2,2))
plot(old.Nm.est$year,old.Nm.est$Median/1000,type='n',las=1,ylab='Abundance (thousands)',main='',xlab='Year',
     xlim=c(1995,2020),ylim=c(20,90));grid()
polygon(x=c(1990,2022,2022,1990),y=c(60,60,80,80),col=rgb(0,1,0,0.1),border=NA)

nyrs=length(old.Nm.est$year)
polygon(x=c(old.Nm.est$year[1:nyrs],old.Nm.est$year[nyrs:1]),
        y=c(old.Nm.est$Lower95[1:nyrs]/1000,old.Nm.est$Upper95[nyrs:1]/1000),
        col=gray(0,.3),border=gray(0,.3))
lines(old.Nm.est$year,old.Nm.est$Median/1000,type='b',pch=16,lwd=1,cex=0.75)


polygon(x=c(1996:2020,2020:1996),
        y=c(tbg.out$q2.5$Nm[1:nyrs]/1000,tbg.out$q97.5$Nm[nyrs:1]/1000),
        col=rgb(255/255,99/255,71/255,0.4),border=rgb(255/255,99/255,71/255,0.4))
lines(1996:2020,tbg.out$q50$Nm[1:nyrs]/1000,type='b',pch=16,lwd=1,cex=0.75,col='red')
legend('bottomleft',legend=c('rossicus included','rossicus excluded'),bty='n',lwd=2,pch=16,col=c('black','red'))
abline(h=70,lty=2,lwd=1)
########################################################################
#####  2021 UPDATE #####################################################
########################################################################
rm(list=ls())
options(max.print = 50000)
library(jagsUI)
setwd('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\European Geese\\B Taiga Bean Geese\\2021 Assessment')
source('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\MyFunctions.R')
load('SubspeciesIPM_jagsUI_12May2021_H2000.RData')

# get posteriors for fixed parameters
attach(tbg.out)
psi = c(q2.5$psi,q50$psi,q97.5$psi)
gamma = c(q2.5$gamma,q50$gamma,q97.5$gamma)
theta = c(q2.5$theta,q50$theta,q97.5$theta)
K = c(q2.5$K,q50$K,q97.5$K)/100000
detach(tbg.out)
parms = data.frame(rbind(psi,gamma,theta,K)); 
plot(1:4,parms[,2],type='n', xaxt='n',ylim=c(0.25,2.75),xlim=c(0,5),xlab='',ylab='Posterior estimate',
     las=1)
abline(h=seq(0,2.75,0.25),lty=3,col='gray')
points(1:4,parms[,2],pch=16,cex=1.1)
axis(1,1:4,labels=c('psi','gamma','theta','K'),)
arrows(1:4,parms[,1],1:4,parms[,3],code=3,angle=90,length=0.1,lwd=1.2)

# monthly pop sizes
Nm = cbind(tbg.out$q2.5$Nm,tbg.out$q50$Nm,tbg.out$q97.5$Nm)
No = cbind(tbg.out$q2.5$No,tbg.out$q50$No,tbg.out$q97.5$No)
Nj = cbind(tbg.out$q2.5$Nj,tbg.out$q50$Nj,tbg.out$q97.5$Nj)

first=1996; lastfall=2024; lastspring=2025
sizeM=nrow(Nm); sizeJO=nrow(Nj)

par(mfrow=c(1,3),mar=c(4,5,2,2))
plot(first:lastspring,Nm[,2]/1000,las=1,type='n',xlab='Year',ylab='Abundance (thousands)',xlim=c(1995,2026),
     ylim=c(30,90),main='March');grid()
polygon(x=c(first:lastspring,lastspring:first),
        y=c(Nm[1:sizeM,1]/1000,Nm[sizeM:1,3]/1000),
        col='lightgray',border='lightgray')
lines(first:lastspring,Nm[,2]/1000,col='black')
points(first:lastspring,Nm[,2]/1000,pch=16,cex=0.75)
abline(v=2021,lty=3);abline(h=70,lty=3)

plot(first:lastfall,No[,2]/1000,las=1,type='n',xlab='Year',ylab='Abundance (thousands)',xlim=c(1995,2026),
     ylim=c(30,90),main='October');grid()
polygon(x=c(first:lastfall,lastfall:first),
        y=c(No[1:sizeJO,1]/1000,No[sizeJO:1,3]/1000),
        col='lightgray',border='lightgray')
lines(first:lastfall,No[,2]/1000,col='black')
points(first:lastfall,No[,2]/1000,pch=16,cex=0.75)
abline(v=2020,lty=3)

plot((first+1):(lastfall+1),Nj[,2]/1000,las=1,type='n',xlab='Year',ylab='Abundance (thousands)',xlim=c(1995,2026),
     ylim=c(30,90),main='January');grid()
polygon(x=c((first+1):(lastfall+1),(lastfall+1):(first+1)),
        y=c(Nj[1:sizeJO,1]/1000,Nj[sizeJO:1,3]/1000),
        col='lightgray',border='lightgray')
lines((first+1):(lastfall+1),Nj[,2]/1000,col='black')
points((first+1):(lastfall+1),Nj[,2]/1000,pch=16,cex=0.75)
abline(v=2021,lty=3)

# harvest rates and apparent survival
h = cbind(tbg.out$q2.5$h,tbg.out$q50$h,tbg.out$q97.5$h)
s = cbind(tbg.out$q2.5$s,tbg.out$q50$s,tbg.out$q97.5$s)

par(mfrow=c(1,2),mar=c(5,5,2,2))
plot(first:lastfall,h[,2],type='n',xlab='Year',ylab='Harvest rate',
     ylim=c(0,0.3),las=1);grid()
polygon(x=c(first:lastfall,lastfall:first),
        y=c(h[1:sizeJO,1],h[sizeJO:1,3]),
        col='lightgray',border='lightgray')
lines(first:lastfall,h[,2],col='black')
points(first:lastfall,h[,2],pch=16,cex=0.75)
abline(v=2020,lty=3)

plot(first:lastfall,s[,2],type='n',xlab='Year',ylab='Survival rate',
     ylim=c(0.6,1),las=1);grid()
polygon(x=c(first:lastfall,lastfall:first),
        y=c(s[1:sizeJO,1],s[sizeJO:1,3]),
        col='lightgray',border='lightgray')
lines(first:lastfall,s[,2],col='black')
points(first:lastfall,s[,2],pch=16,cex=0.75)
abline(v=2020,lty=3)

# country harvests
Hf = cbind(tbg.out$q2.5$Hf,tbg.out$q50$Hf,tbg.out$q97.5$Hf)
Hs = cbind(tbg.out$q2.5$Hs,tbg.out$q50$Hs,tbg.out$q97.5$Hs)
Hd = cbind(tbg.out$q2.5$Hd,tbg.out$q50$Hd,tbg.out$q97.5$Hd)

par(mfrow=c(1,3),mar=c(5,5,2,2))
plot(first:lastfall,Hf[,2]/1000,las=1,type='n',xlab='Year',ylab='Harvest (thousands)',
     ylim=c(0,10),main='Finland');grid()
polygon(x=c(first:lastfall,lastfall:first),
        y=c(Hf[1:sizeJO,1]/1000,Hf[sizeJO:1,3]/1000),
        col='lightgray',border='lightgray')
lines(first:lastfall,Hf[,2]/1000,col='black')
points(first:lastfall,Hf[,2]/1000,pch=16,cex=0.75)
abline(v=2020,lty=3)

plot(first:lastfall,Hs[,2]/1000,las=1,type='n',xlab='Year',ylab='Harvest (thousands)',
     ylim=c(0,10),main='Sweden');grid()
polygon(x=c(first:lastfall,lastfall:first),
        y=c(Hs[1:sizeJO,1]/1000,Hs[sizeJO:1,3]/1000),
        col='lightgray',border='lightgray')
lines(first:lastfall,Hs[,2]/1000,col='black')
points(first:lastfall,Hs[,2]/1000,pch=16,cex=0.75)
abline(v=2019,lty=3)

plot(first:lastfall,Hd[,2]/1000,las=1,type='n',xlab='Year',ylab='Harvest (thousands)',
     ylim=c(0,10),main='Denmark');grid()
polygon(x=c(first:lastfall,lastfall:first),
        y=c(Hd[1:sizeJO,1]/1000,Hd[sizeJO:1,3]/1000),
        col='lightgray',border='lightgray')
lines(first:lastfall,Hd[,2]/1000,col='black')
points(first:lastfall,Hd[,2]/1000,pch=16,cex=0.75)
abline(v=2020,lty=3)

#############################################################################
# Equilibrium analyses
#############################################################################
psi=tbg.out$sims.list$psi
gamma=tbg.out$sims.list$gamma
K=tbg.out$sims.list$K
theta=tbg.out$sims.list$theta


# calculate realized r
r.fun = function(psi,gamma,K,theta,N) {(psi+psi*gamma-1)*(1-(N/K)^theta)}

N = seq(0,100000,50)
l = length(N)
r = matrix(nrow=length(psi),ncol=length(N))
for (i in 1:l) {
        r[,i] = r.fun(psi,gamma,K,theta,N[i]) 
}

q.fun = function(x) quantile(x, probs=c(0.025,0.5,0.975))
r.q = apply(r,2,q.fun)
N = N/1000

fun = function(x) median(x)
#Nmsy = fun(K)*(fun(theta)+1)^(-1/fun(theta))
Nmsy = (K)*((theta)+1)^(-1/(theta))
rmax = ((psi)+(psi)*(gamma)-1)
Hmsy = rmax*(K)*(theta) / ( ((theta)+1)^ (((theta)+1)/(theta) ))

q.fun(Nmsy)
q.fun(rmax)
q.fun(Hmsy)

# w = which(N==round(median(Nmsy)/1000,1))
# N[w]*r.q[1,w]
# N[w]*r.q[2,w]
# N[w]*r.q[3,w]

w = which(N==70)
N[w]*r.q[1,w]
N[w]*r.q[2,w]
N[w]*r.q[3,w]

w = which(N==66.90)
N[w]*r.q[1,w]
N[w]*r.q[2,w]
N[w]*r.q[3,w]

par(mfrow=c(1,2),mar=c(5,4,2,2))
plot(N,r.q[2,],type='n',las=1,xlab='Abundance (thousands)',ylab='Realized growth rate',
     xaxp=c(0,100,4),ylim=c(-0.25,0.4))
polygon(x=c(N[1:l],N[l:1]),y=c(r.q[1,1:l],r.q[3,l:1]),col='gray90',border='gray90')
lines(N,r.q[2,],lwd=2)
abline(h=seq(-0.2,0.4,.1),v=seq(0,100,25),col='lightgray',lty=2)
abline(h=0,lty=2)

plot(N,N*r.q[2,],type='n',las=1,xlab='Abundance (thousands)',ylab='Surplus production (thousands)',
     ylim=c(-21,10),xaxp=c(0,100,4))
polygon(x=c(N[1:l],N[l:1]),y=c(N[1:l]*r.q[1,1:l],N[l:1]*r.q[3,l:1]),col='gray90',border='gray90')
lines(N,N*r.q[2,],lwd=2)
# lines(N,N*r.q[1,])
# lines(N,N*r.q[3,])
abline(h=seq(-20,10,5),v=seq(0,100,25),col='lightgray',lty=2)
abline(h=0,lty=2)
abline(v=70, h=N[w]*r.q[2,w], lty=3,col='red',lwd=2)








