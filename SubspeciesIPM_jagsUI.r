# SubspeciesIPM.R
# deterministic model 
# with projections of pop size 5 years into future

rm(list=ls())
options(max.print = 50000)
library(jagsUI)
setwd('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\European Geese\\B Taiga Bean Geese\\2021 Assessment')
source('C:\\Users\\fjohn\\OneDrive\\Documents\\PROJECTS\\MyFunctions.R')

#####################################################################
### COMPILE DATA ####################################################
#####################################################################
# all data rounded to the nearest 100
IPMdata = read.csv('2021 DATA.csv')

# align january count with population year (Jan temp is already aligned correctly)
IPMdata$jan=c(IPMdata$jan[-1],NA)

nyears = length(1996:2021)
future = 4

#... prior for initial March population size
m.init = lognorm.pr(50000,10000) # 1996 March pop & large CV (95% CI 27 - 63k)
lmean.m = m.init[1]
ltau.m = 1/m.init[2]^2

attach(IPMdata)
jags.data <- list(nyears=nyears, future=future,
                  Mar=mar, Oct=oct, Jan=jan,
                  Hden=hden, Hswe=hswe, Hfin=hfin,
                  lmean.m=lmean.m, ltau.m=ltau.m)
detach(IPMdata)

#####################################################################
### PREPARE MODEL ###################################################
#####################################################################
# initial values
init.vals <- function(){list(psi=0.9,
                             gamma=0.4,
                             K=83000,
                             theta=1.7,
                             alpha=513/(513+224),
                             mu.pmtaiga=0.9,
                             sigma.pmtaiga=0.5,
                             mu.potaiga=0.9,
                             sigma.potaiga=1,
                             Hf=rep(3000,nyears-1+future),
                             Hs=rep(3000,nyears-1+future),
                             Hd=rep(1000,nyears-1+future)
                             )}

# parameters to monitor
parms = c('Nm','No','Nj',
          'Hf','Hs','Hd','H','h',
          'psi','gamma','K','theta',
          'tau.m','tau.o','tau.j',
          'alpha','lam','s',
          'mu.pmtaiga','mu.potaiga',
          'sigma.pmtaiga','sigma.potaiga')


# Define model 
#tbg.mod <- "model{
cat(file="tbg.mod",
    "model {

# PRIORS ------------------------------------------------------------

#... initial March pop size for 1996
Nm[1] ~ dlnorm(lmean.m,ltau.m)

#... observation (residual) errors
sigma.m ~ dunif(0,1)
 tau.m <- 1/sigma.m^2 # March count residual error
sigma.o ~ dunif(0,1)
 tau.o <- 1/sigma.o^2 # Oct count residual error
sigma.j ~ dunif(0,1)
 tau.j <- 1/sigma.j^2 # Jan count residual error

#... harvest priors 
for (t in 1:(nyears-1)) {
    Hf[t] ~ dunif(0,10000)
    Hd[t] ~ dunif(0,10000)
    Hs[t] ~ dunif(0,5000)
 }

# #... H = 3000 and nominal allocation (58%, 30%, 12%) 
# for (t in nyears:(nyears+future-1)) {
#     Hf[t] ~ dpois(1700)
#     Hs[t] ~ dpois(900)
#     Hd[t] ~ dpois(400)
# }

# #... H = 2300 and nominal allocation (58%, 30%, 12%) 
# for (t in nyears:(nyears+future-1)) {
#     Hf[t] ~ dpois(1334)
#     Hs[t] ~ dpois(690)
#     Hd[t] ~ dpois(276)
# }

#... H = 2000 and nominal allocation (58%, 30%, 12%) 
for (t in nyears:(nyears+future-1)) {
    Hf[t] ~ dpois(1160)
    Hs[t] ~ dpois(600)
    Hd[t] ~ dpois(240)
}
# portion of Swedish harvest prior to Jan count
alpha ~ dbeta(513,224) # from Nicklas 2017 & 2018 prior to Jan 15

# priors from Prior_Distributions.R
#... psi
 psi ~ dbeta(58.17,7.89)
#... gamma 
 gamma ~ dlnorm(-1.21,1/0.26^2)
#... K
 K ~ dlnorm(11.38,1/0.04^2)
#... theta
# theta ~ dlnorm(0.85,1/0.79^2) # original
 theta ~ dlnorm(0.85,1/0.3^2) # more informative 

# priors to account for portions of taigas
#... March counts
mu.pmtaiga ~ dbeta(6726,506)  # mu=0.930, se = 0.003
logit.mu.pmtaiga <- log(mu.pmtaiga/(1-mu.pmtaiga))
sigma.pmtaiga ~ dunif(0,1)    # temporal variance logit scale
tau.pmtaiga <- 1/sigma.pmtaiga^2
for (t in 1:(nyears+future)) {
  e.pmtaiga[t] ~ dnorm(0,tau.pmtaiga)
  logit.pmtaiga[t] <- logit.mu.pmtaiga + e.pmtaiga[t]
  pmtaiga[t] <- exp(logit.pmtaiga[t])/(1+exp(logit.pmtaiga[t]))
}

#... October counts
mu.potaiga ~ dbeta(137,17)  # mu=0.891, se = 0.025
logit.mu.potaiga <- log(mu.potaiga/(1-mu.potaiga))
sigma.potaiga ~ dunif(0,2)    # temporal variance logit scale (upper limit of 2 provides similar SD to data)
tau.potaiga <- 1/sigma.potaiga^2
for (t in 1:(nyears-1+future)) {
  e.potaiga[t] ~ dnorm(0,tau.potaiga)
  logit.potaiga[t] <- logit.mu.potaiga + e.potaiga[t]
  potaiga[t] <- exp(logit.potaiga[t])/(1+exp(logit.potaiga[t]))
}


# System process --------------------------------------------------

for (t in 1:(nyears-1+future)) {
    No[t] <- max(0.001, Nm[t] + Nm[t]*((psi^(7/12)*(1+gamma)-1)*(1-(Nm[t]/K)^theta)) - Hf[t])
}

for (t in 1:(nyears-1+future)) {
    Nj[t] <- max(0.001, (No[t]-Hd[t]-alpha*Hs[t])*psi^(3/12))
}

for (t in 2:(nyears+future)) {
    Nm[t] <- max(0.001, (Nj[t-1]-(1-alpha)*Hs[t-1])*psi^(2/12))
}


# Likelihood of pops -------------------------------------------

#... March counts 2007-2021 (missing 1996-2006, 2010, 2013)
for (t in 12:14) {
    Mar[t] ~ dlnorm(log(Nm[t]/pmtaiga[t])-0.5/tau.m,tau.m)
}
for (t in 16:17) {
    Mar[t] ~ dlnorm(log(Nm[t]/pmtaiga[t])-0.5/tau.m,tau.m)
}
for (t in 19:nyears) {
    Mar[t] ~ dlnorm(log(Nm[t]/pmtaiga[t])-0.5/tau.m,tau.m)
}

#... October counts (1996-2015 are to be adjusted)
for (t in 1:20) {
    Oct[t] ~ dlnorm(log(No[t]/potaiga[t])-0.5/tau.o,tau.o)
}
for (t in 21:24) {
    Oct[t] ~ dlnorm(log(No[t])-0.5/tau.o,tau.o)
}

#... January counts (all years preprocessed, Germany still missing)
for (t in 1:24) {
    Jan[t] ~ dlnorm(log(Nj[t])-0.5/tau.j,tau.j)
}


# Likelihood of harvests --------------------------------------

#... Finland (all data preprocessed)
for (t in 1:(nyears-1)) {
    finLam[t] <- max(0.1,Hf[t])
    Hfin[t] ~ dpois(finLam[t])
}

#... Sweden (only 2016-2018 preprocessed; 2020 missing)
for (t in 1:20) {
    sweLam[t] <- max(0.1,Hs[t])
    Hswe[t] ~ dpois(sweLam[t]/potaiga[t])
}
for (t in 21:24) {
    sweLam[t] <- max(0.1,Hs[t])
    Hswe[t] ~ dpois(sweLam[t])
}

#... Denmark (all data preprocessed)
for (t in 1:(nyears-1)) {
    denLam[t] <- max(0.1,Hd[t])
    Hden[t] ~ dpois(denLam[t])
}


# Derived parms -----------------------------------------------

for (t in 1:(nyears-1+future)) {
    H[t] <- (Hf[t] + Hs[t] + Hd[t])
    h[t] <- (Hf[t] + Hs[t] + Hd[t]) /
            (Nm[t]*psi^(7/12) + Nm[t]*((psi^(7/12)*(1+gamma)-1)*(1-(Nm[t]/K)^theta)))
    }

for (t in 1:(nyears-1+future)) {
    lam[t] <- Nm[t+1]/Nm[t] # realized finite growth rate
    s[t] <- psi * (1-h[t])  # apparent survival (no DD or environmental effects)
    }
} # End model
")

################################################################
## RUN JAGS ####################################################
################################################################
ni <- 1100000
nb <-  1050000
nc <- 3 # number of chains

start = proc.time()[3]
tbg.out <- jags(jags.data, init.vals, parms, "tbg.mod", n.chains = nc, n.burnin = nb, 
                n.iter = ni, parallel = TRUE)
(time = (proc.time()[3]-start)/60)

save.image('SubspeciesIPM_jagsUI_12May2021_H2000.RData')
